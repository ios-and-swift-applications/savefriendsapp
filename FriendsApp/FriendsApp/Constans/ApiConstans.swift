//
//  ApiConstans.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/17/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation

class ApiConstans {
    
    static var UrlRooApi : String = "https://friendsappbackend.azurewebsites.net/api/"
    static var login: String = UrlRooApi+"Login"
    static var registerUser : String = UrlRooApi+"RegisterAccount"
    static var createGroup : String = UrlRooApi+"CreatedGroup"
    static var GetAllGroups :String = UrlRooApi+"GetAllgroups"
    static var GeAllFriends : String = UrlRooApi+"GetAllFriends"
    static var getGroupById : String = UrlRooApi+"GetGroupById"
    static var getFriendById : String = UrlRooApi+"GetFriendById"
    static var updateInfoGroup : String = UrlRooApi+""
    static var joinMeInGroup : String = UrlRooApi+"AddInNewGroup"
    
}
