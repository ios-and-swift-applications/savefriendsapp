//
//  HeaderGroupCollectionReusableView.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/24/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import UIKit

class HeaderGroupCollectionReusableView: UICollectionReusableView {
   
    @IBOutlet weak var headerStackView: UIStackView!
    
    @IBOutlet weak var lbTitleHeader: UILabel!
    
    @IBOutlet weak var lbNumberOfGroups: UILabel!
    
    
    
}
