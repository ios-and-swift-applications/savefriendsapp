//
//  GroupCollectionViewCell.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/24/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import UIKit

class GroupCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var ViewGroupCell: UIView!
    @IBOutlet weak var imgGroup: UIImageView!
    @IBOutlet weak var lblNameGroup: UILabel!
    
    
    func configureCollectionCell() -> Void {
        contentView.backgroundColor = UIColor.white
        
        ViewGroupCell.backgroundColor = UIColor.white
        ViewGroupCell.layer.masksToBounds = false
        ViewGroupCell.layer.cornerRadius = 5.0
        
        ViewGroupCell.layer.shadowColor = UIColor(red: 0.0, green: 0.0, blue: 1.0, alpha: 0.2).cgColor
        ViewGroupCell.layer.shadowOffset = CGSize(width: 0, height: 0)
        ViewGroupCell.layer.shadowOpacity = 0.8
        
        
        
    }
    
}
