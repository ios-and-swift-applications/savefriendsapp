//
//  FriendCollectionViewCell.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/24/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import UIKit

class FriendCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var groupViewCell: UIView!
    @IBOutlet weak var lnNombre: UILabel!
    
    @IBOutlet weak var GroupImage: UIImageView!
    
    @IBOutlet weak var btnSeedFriend: UIButton!
    
    
    func ConfigureCell() -> Void {
        
        contentView.backgroundColor = UIColor.white
        
        groupViewCell.backgroundColor = UIColor.white
        groupViewCell.layer.masksToBounds = false
        groupViewCell.layer.cornerRadius = 5.0
        groupViewCell.layer.shadowOffset = CGSize(width: 0, height: 0)
        groupViewCell.layer.shadowColor = UIColor(red: 0, green:0, blue: 1.0, alpha: 0.2).cgColor
        groupViewCell.layer.shadowOpacity = 0.8
        btnSeedFriend.layer.cornerRadius = 20.0
        
        
        
    }
    
    
}
