//
//  MyActionsTableViewCell.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/30/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import UIKit

class MyActionsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var iconAction: UIImageView!
    
    @IBOutlet weak var titleAction: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }



}
