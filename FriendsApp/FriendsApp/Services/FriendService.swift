//
//  FriendService.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/15/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class FriendService {
    
    func login(email:String,password:String, completion :  @escaping(Any?)-> Void) {
    
       let parameters : Parameters = ["email":email,"password":password]
        let url:String = ApiConstans.login
        let headers:HTTPHeaders = ["Content-Type":"application/json"]
        Alamofire.request(url, method: .get, parameters: parameters, headers: headers).responseObject(keyPath:"result"){
            (response:DataResponse<Friend>) in
            switch response.result{
            case .success(let data):
                print(data)
                DispatchQueue.main.async{
                    completion(data)
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    completion(error)
                }
            }
            
        }
    
    
    }
    

    
    
    func registerFriend(newUser:Friend, completion : @escaping(ApiResponse)-> Void){
        
        var nuevoUser: Friend = Friend()
        nuevoUser = newUser
        
        guard let nameUser  = newUser.name,
            let ageUser = newUser.age,
            let emailUser = newUser.email,
            let passwordUser = newUser.password
        else {
            return
        }
    
        
        let parameters : [String:String] = [
            "Name": nameUser,
            "CellPhone":"",
            "Email": emailUser,
            "Password": passwordUser,
            "Age": ageUser,
            "UrlPhoto":""
        ]
        
        Alamofire.request(ApiConstans.registerUser, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseObject(){
            (response:DataResponse<ApiResponse>) in
            switch response.result{
            case .success:
                print(response.result.value!)
                DispatchQueue.main.async{
                    completion(response.result.value!)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    func createGroup(newGroup:Group, completion:@escaping(ApiResponse) -> Void){
        let parameters : Parameters = ["group":newGroup]
        Alamofire.request(ApiConstans.createGroup, method: .post, parameters:parameters,encoding:JSONEncoding.default,headers:nil).responseObject {
            (response:DataResponse<ApiResponse>) in
            switch response.result{
            case .success:
                print(response)
                DispatchQueue.main.async{
                    completion(response.result.value!)
                }
            case .failure(let error):
                print(error)
            }
        }
    }

    func joinMeInGroup(secretKey:String, idFriend:String, completion:@escaping(ApiResponse) -> Void){
        
        let params : Parameters = ["idFriend":idFriend,"secretGroupKey":secretKey]
        Alamofire.request(ApiConstans.joinMeInGroup, method: .get, parameters: params,  headers: nil).responseObject {
            (response:DataResponse<ApiResponse>) in
            switch response.result{
            case .success:
                print(response.result.value)
                DispatchQueue.main.async{
                    completion(response.result.value!)
                }
            case .failure(let error):
                print(error)
            }

        }
    }
}


