//
//  GroupService.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/22/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class GroupService {
    
    var header : HTTPHeaders = ["Content-type":"application/josn","Accept":"application/josn"]
    
    func GetAllGroups(completion:@escaping([Group])-> Void){
        
        Alamofire.request(ApiConstans.GetAllGroups, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.header).responseArray(keyPath:"result"){
            (response:DataResponse<[Group]>) in
            switch response.result{
            case .success(let data):
                DispatchQueue.main.async {
                    completion(data)
                }
            case .failure(let error):
                print("Ocurrio un error \(error)")
            }
        }
    }
    
    func getGroupById(idGroup:String,completion : @escaping(Group)->Void){
        
        let params : Parameters = ["idGroup":idGroup]
        
        Alamofire.request(ApiConstans.getGroupById, method: .get, parameters: params, headers: self.header).responseObject(keyPath:"result"){
            (response:DataResponse<Group>) in
            switch response.result{
            case .success(let data):
                print(data)
                DispatchQueue.main.async {
                    completion(data)
                }
            case .failure(let error):
                print("Ocurrio un error \(error)")
            }
        }
        
    }
    
    func updateInfoGroup(group:Group,completio:@escaping(ApiResponse)-> Void){
        
        Alamofire.request(ApiConstans.updateInfoGroup, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.header).responseObject(keyPath:"result"){
            (response:DataResponse<ApiResponse>) in
            switch response.result{
            case .success(let data):
                print(data)
            case .failure(let error):
                print(error)
            }
            
        }
        
        
    }
    
}
