//
//  InsertInGroupPresenter.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/25/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation

class InsertInGroupPresenter: InserInGroupViewPresenter {
    
    private var inserInGroupView:InserInGroupView!
    private var friendService:FriendService!
    
    required init(view: InserInGroupView) {
        self.inserInGroupView = view
        self.friendService = FriendService()
    }
    
    func joinMeInGroup(secretKey: String, idUser: String) {
        
        self.inserInGroupView.showActivityIndicator()
        self.friendService.joinMeInGroup(secretKey: secretKey, idFriend: idUser){[weak self] result in
            
            guard let responseResult = result as? ApiResponse else{
                self?.inserInGroupView.hideActivitiIndicator()
                  self?.inserInGroupView.showMessageInsertFailure(title: "Ocurrio un error", message: "Ocurrio un error en el registro al grupo intente màs tarde.")
            }
            guard let messageText = result.result as? String else {
                return
            }
            guard let statusCode = result.statudCode as? String else{
                return
            }
            if statusCode != "200"{
                self?.inserInGroupView.hideActivitiIndicator()
                self?.inserInGroupView.showMessageInsertFailure(title: "error", message: messageText)
                return
            }
            self?.inserInGroupView.hideActivitiIndicator()
            self?.inserInGroupView.showMessageInsertSuccesfully(title: "Ingreso correcto", message: messageText)
        }
        
        
    }
    
    
    
    
    
}
