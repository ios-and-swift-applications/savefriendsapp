//
//  LoginPresenter.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/15/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation
import UIKit
class LoginPresenter : LoginViewPresenter{

        
    private var  loginView:LoginView!
    private var friendService:FriendService!
    
    required init(view: LoginView) {
        self.loginView = view
        self.friendService = FriendService()
    }
    
    func login(email:String,password:String) -> Void{
        
        let validEmailPassworsIsValid = ValidateEmailAndPassword(email:email,password:password)
        var userLogin:Friend?
        if validEmailPassworsIsValid{
            self.loginView.startActivityIndicator()
            self.friendService.login(email: email, password: password){[weak self] userResult in
                
                guard let result = userResult as? Friend else{
                    self?.loginView.stopActivityIndicator()
                    self?.loginView.showMessage(title:"Usuario no encontrado",message: "el usuario no se ha encontrado en nuestro sistema, por favor registrese")
                    return
                }
                
                NotificationCenter.default.post(name: NSNotification.Name("infoFriendLogin"), object: result)
                self?.loginView.stopActivityIndicator()
                self?.loginView.gotoHomeView(friend: result)
            }
        }else{
            loginView.showMessage(title:"",message: "El usuario o contraseña son incorrectos")
        }
    }
    
    func gotoRegisterView()-> Void{
        self.loginView.gotoRegisterView();
    }
    
    func ValidateEmailAndPassword(email:String?,password:String?) -> Bool {
        let emailAndPassworIsValid = false
        guard let _ = email,
            let _ = password else {
                return emailAndPassworIsValid
        }
        return true
    }
}
