//
//  DetailGroupPresenter.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/24/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation

class DetailGroupPresenter: DetailGroupViewPresenter {
   
    
    private var groupService:GroupService!
    private var detailGroupView:DetailGroupView
    
    required init(view: DetailGroupView) {
        self.detailGroupView = view
        self.groupService = GroupService()
    }
    
    func getGroupById(idGroup: String) {
        groupService.getGroupById(idGroup: idGroup){[weak self] grupoResult in
            print("resutado", grupoResult)
            self?.detailGroupView.showGroupInTheInterface(groupfound: grupoResult)
        }
    }
    
    func joinMeInGroup(secretKey: String, idFriend: String) {
        
    }
    
    
}
