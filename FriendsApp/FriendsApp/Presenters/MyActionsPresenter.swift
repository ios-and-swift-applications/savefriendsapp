//
//  MyActionsPresenter.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/26/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation

class MyActionsPresenter {
    
    private var myActionsView : MyActionsView!
    
    init(view:MyActionsView) {
        self.myActionsView = view
    }
    
    func GetMyActions() -> [MyActions] {
        
        var myActions : [MyActions] = []
        var miGroups = MyActions.init(name: "Mis grupos", icon: "misGrupos.png")
        var joinMeInGroup = MyActions.init(name: "Unirme a un grupo", icon: "joinmeGroup.png")
        var myPerfil = MyActions.init(name: "Mi perfil", icon: "MyProfile.png")
        myActions.append(myPerfil)
        myActions.append(miGroups)
        myActions.append(joinMeInGroup)
        
        return myActions
        
    }
    
    func goToMyGroups() -> Void {
        print("Hola")
    }
    
    func goMyProfile() -> Void {
        print("Hola")
    }
    
    func goToJoinInGroup() -> Void {
        
        print("Hola")
    }
    
    
}

