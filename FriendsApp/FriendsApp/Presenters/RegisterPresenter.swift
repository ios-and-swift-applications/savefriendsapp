//
//  RegisterPresenter.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/17/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation

class RegisterPresenter : RegisterViewPresenter {
    
    private var registerView:RegisterView
    private var friendService: FriendService!
    required init(view:RegisterView){
        self.registerView = view
        self.friendService = FriendService()
        
    }
    
    func RegisterUSer(user:Friend) -> Void {
        
        guard let passwordValidAux = user.password,
              let emailAux = user.email else{
            return
        }
        
        var passwordValid = IsValidLongitudPassword(password: passwordValidAux)
        var emailValid = IsValidEmailFormat(email: emailAux)
        if !passwordValid{
            showAlert(title:"longitud de contraseña corta", message: "La contraseña debe ser mayor a 8 diitos")
            return
        }
        if !emailValid {
            showAlert(title:"El correo debe tener un formato valido",message: "El email no tiene un formato valido")
            return
        }
        friendService.registerFriend(newUser: user){[weak self] resulResponse in
            
            if resulResponse.statudCode == "200"{
                self?.showAlert(title:"Registro existoso",message: "usuario registrado con exito")
                
            }else{
                self?.showAlert(title:"error en el registro",message: "Ocurrio un error intente màs tarde")
            }
        }
        
    }
    
    func IsValidLongitudPassword(password:String) -> Bool {
        
        if password.count < 8 {
            return false
        }else{
            return true
        }
        
    }
    
    func IsValidEmailFormat(email:String) -> Bool{
        
        if email.count > 0{
            return true
        }else{
            return false
        }
        
    }
    
    func showAlert(title:String, message:String) -> Void{
        registerView.showAlert(title:title,message: message)
        
    }
    
    
}
