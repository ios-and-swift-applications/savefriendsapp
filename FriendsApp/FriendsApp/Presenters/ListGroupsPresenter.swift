//
//  ListGroupsPresenter.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/22/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation
import UIKit
class ListGroupsPresenter: ListGroupsViewPresenter {
    
    private var listGroupsView : ListGroupsView
    private var groupService : GroupService!
    required init(view: ListGroupsView) {
        self.listGroupsView = view
        self.groupService = GroupService();
    }
    
    func getListGroups() {
        
        var listGroups : [Group] = []
        
        listGroupsView.showActivityIndicator()
        groupService.GetAllGroups(){[weak self] listaGroups in
            
            listGroups = listaGroups
            print("lista de grupos")
            print(listGroups)
            self?.listGroupsView.hideActivityIndicator()
            self?.listGroupsView.ShowData(listaGroups: listGroups)
        }
    
    }
    
    func getGroupById(idGroup: String){
        
        var groupFound:Group?
        groupService.getGroupById(idGroup: idGroup){[weak self] grupoResult in
            
            
        }
    
    }
    
    func viewGroupDetail(groupId: String) {
        
        listGroupsView.gotoDeailGroupView(groupId: groupId)
        
    }
    
    
    
    
    
    
}
