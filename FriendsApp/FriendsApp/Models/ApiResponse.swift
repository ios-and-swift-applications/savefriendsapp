//
//  ApiResponse.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/18/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation
import ObjectMapper

class ApiResponse : Mappable {

    var result:Any?
    var statudCode:String?
    
    
    required init?(map:Map) {
        self.mapping(map: map)
    }
    
    init() {
        
    }
    func mapping(map: Map) {
        result <- map["result"]
        statudCode <- map["statudCode"]
    }
    

    
    

}
