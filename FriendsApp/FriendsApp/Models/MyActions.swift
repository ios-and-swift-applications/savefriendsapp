//
//  MyActions.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/26/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation

struct MyActions {
    
    init(name:String,icon:String) {
        self.Name = name
        self.Icon = icon
    }
    
    var Name:String?
    var Icon:String?
    
}
