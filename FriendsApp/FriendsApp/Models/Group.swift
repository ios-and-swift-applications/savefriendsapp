//
//  Group.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/15/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation
import ObjectMapper

class Group : Mappable{
    
    var id:String?
    var name:String?
    var description:String?
    var creationDate:Date?
    var friends:[Friend]?
    var secrectKey:String?
    
    required init?(map: Map){
        mapping(map:map)
    }
    
    init() {
        
    }
    
    
     func mapping(map:Map){
        id <- map["identifier"]
        name <- map["name"]
        description <- map["description"]
        creationDate <- map["creationDate"]
        friends <- map["friends"]
        secrectKey <- map["secretKey"]
    }
}

