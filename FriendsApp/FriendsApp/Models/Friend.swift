//
//  Friend.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/15/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation
import ObjectMapper

class Friend : Mappable {
    var age:String?
    var urlPhoto:String?
    var id:String?
    var name:String?
    var cellphone:String?
    var email:String?
    var password:String?
    
    required init?(map:Map){
        mapping(map:map)
    }
    
    init() {
        
    }
    
    func mapping(map:Map){
        self.age <- map["age"]
        self.urlPhoto <- map["urlPhoto"]
        self.id <- map["id"]
        self.name <- map["name"]
        self.cellphone <- map["cellPhone"]
        self.email <- map["email"]
        self.password <- map["password"]

    }
}
