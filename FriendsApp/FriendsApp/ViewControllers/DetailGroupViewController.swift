//
//  DetailGroupViewController.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/15/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import UIKit

class DetailGroupViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet weak var GroupImageView: UIView!
    @IBOutlet weak var mainImageGroup: UIImageView!
    @IBOutlet weak var infoGroupView: UIView!
    @IBOutlet weak var lblGroupName: UILabel!
    @IBOutlet weak var lblGroupNameValue: UILabel!
    @IBOutlet weak var lblGroupDescription: UILabel!
    @IBOutlet weak var FriendsGroupView: UIView!
    @IBOutlet weak var friendsGroupCollection: UICollectionView!
    
    @IBOutlet weak var btnJoinInGroup: UIButton!
    @IBOutlet weak var joinInGroupView: UIView!
    
    var idGroup:String = ""
    private var listFriendsInGroup : [Friend] = []
    private var detatilGroupPresenter : DetailGroupPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        friendsGroupCollection.delegate = self
        friendsGroupCollection.dataSource = self
        detatilGroupPresenter = DetailGroupPresenter(view: self)
        detatilGroupPresenter.getGroupById(idGroup: idGroup)
    }
    
 
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(listFriendsInGroup.count)
        return listFriendsInGroup.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let friendcollectionCell = friendsGroupCollection.dequeueReusableCell(withReuseIdentifier: "friendCollectionCell", for: indexPath) as! FriendCollectionViewCell
        friendcollectionCell.lnNombre.text = listFriendsInGroup[indexPath.row].name
        friendcollectionCell.ConfigureCell()
        
        return friendcollectionCell
        
    }
    

    

}

extension DetailGroupViewController : DetailGroupView{
    
    func showGroupInTheInterface(groupfound: Group) {
        
        mainImageGroup.image = UIImage(named: "NodeJsLogo.png")
        lblGroupNameValue.text = groupfound.name
        lblGroupDescription.text = groupfound.description
        
        guard  let listaGrupos = groupfound.friends else {
            return
        }
        listFriendsInGroup = listaGrupos
        print(listFriendsInGroup.count)
        friendsGroupCollection.reloadData()
    }
    
    
    
    
    
}
