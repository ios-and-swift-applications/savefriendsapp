//
//  MyAcctionsViewController.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/25/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import UIKit

class MyAcctionsViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var myActionsTableView: UITableView!
    
    private var myActionsPresenter:MyActionsPresenter!
    private var listMyActions:[MyActions] = []
    var dataTest:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        myActionsTableView.delegate = self
        myActionsTableView.dataSource = self
        self.myActionsPresenter = MyActionsPresenter(view: self)
        self.suscribeNotificationFriendLogin()
        self.ShowListMyActions()
        // Do any additional setup after loading the view.
    }
    
    private func suscribeNotificationFriendLogin()-> Void{
        NotificationCenter.default.addObserver(self, selector: #selector(recivedUserInfo), name: NSNotification.Name("infoFriendLogin"), object: nil)
        
    }
    
    @objc func recivedUserInfo(notification:Notification)-> Void{
        
        guard let userReceived = notification.object as? Friend else {
            return
        }
        print("Este es e user recibido ")
        print(userReceived)
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listMyActions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let  myActionCell = myActionsTableView.dequeueReusableCell(withIdentifier: "myActionsViewCells", for: indexPath) as! MyActionsTableViewCell
        
        myActionCell.titleAction.text = listMyActions[indexPath.row].Name
        myActionCell.iconAction.image = UIImage(named: listMyActions[indexPath.row].Icon!)
        return myActionCell
    }
}

extension MyAcctionsViewController : MyActionsView{
    func gotoMyGroups() {
        
        
    }
    
    func gotoJoinInGroup() {
        
    }
    
    func gotoGroupList() {
        
    }
    
    func ShowListMyActions() {
        
        self.listMyActions = self.myActionsPresenter.GetMyActions()
        myActionsTableView.reloadData()
        
    }
    
    
    
}
