//
//  FirstViewController.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/15/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import UIKit

class GroupListViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var gruposCollection: UICollectionView!
    private var listGruposPresenter:ListGroupsPresenter!
    private var activityIndicatorUtil: ActivityIndicatorUitil!
    var actvitiIndicatorView: UIActivityIndicatorView?
    var listaDataGrupos:[Group] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityIndicatorUtil = ActivityIndicatorUitil()
        self.gruposCollection.dataSource = self
        self.gruposCollection.delegate = self
        listGruposPresenter = ListGroupsPresenter(view: self)
        listGruposPresenter.getListGroups()

    }
    
    private func createActivityIndicatorFrame()-> UIActivityIndicatorView{
        let activityIndView = self.activityIndicatorUtil.createActiviyIndicator(view: view)
        return activityIndView
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listaDataGrupos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.gruposCollection.dequeueReusableCell(withReuseIdentifier: "groupCollectionCell", for: indexPath) as! GroupCollectionViewCell
        
        cell.imgGroup?.image = UIImage(named: "NodeJsLogo.png")
        cell.lblNameGroup.text = listaDataGrupos[indexPath.row].name
        
        cell.configureCollectionCell()
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let idGroup = listaDataGrupos[indexPath.row].id else{
            return
        }
        self.listGruposPresenter.viewGroupDetail(groupId: idGroup)
    }
    
    /* MARK header of UICollectionView*/
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var headerGroupView : HeaderGroupCollectionReusableView!
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            headerGroupView = gruposCollection.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headerListGroupCollectionView", for: indexPath) as? HeaderGroupCollectionReusableView
            
            headerGroupView.lbTitleHeader.text = "Numero de grupos:"
            headerGroupView.lbNumberOfGroups.text = String(listaDataGrupos.count)
            return headerGroupView
            
        default:
            print("No hay header")
        }
        return headerGroupView
    }
    
    
    


}



extension GroupListViewController : ListGroupsView{

    
    
    func showActivityIndicator() {
        self.actvitiIndicatorView = createActivityIndicatorFrame()
        self.actvitiIndicatorView?.startAnimating()
    }
    
    func hideActivityIndicator() {
        self.actvitiIndicatorView?.stopAnimating()
        self.activityIndicatorUtil.removeActiviIndicatorView(view: view)
    }
    
    func ShowData(listaGroups: [Group]) {
        
        self.listaDataGrupos = listaGroups
        self.gruposCollection.reloadData()
        
    }
    
    func gotoDeailGroupView(groupId:String) {
        
        var vc = storyboard?.instantiateViewController(withIdentifier: "DetailGroupViewController") as! DetailGroupViewController
        vc.idGroup = groupId
        navigationController?.pushViewController(vc, animated: true)
        
    
    }
     
    
}

