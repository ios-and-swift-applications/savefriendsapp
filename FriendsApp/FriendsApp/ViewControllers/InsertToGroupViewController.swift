//
//  SecondViewController.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/15/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import UIKit

class InsertToGroupViewController: UIViewController {


    @IBOutlet weak var txtSecretKeyGroup: UITextField!
    @IBOutlet weak var btnJoinGroupPressed: UIButton!
    private var joingGroupPresenter:InsertInGroupPresenter!
    private var activiIndicatorView:UIActivityIndicatorView!
    private var activiIndicctorUtil: ActivityIndicatorUitil!
    
    private var idFriend : String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.suscribeToNotificationUserLogin()
        self.activiIndicctorUtil = ActivityIndicatorUitil()
        self.joingGroupPresenter = InsertInGroupPresenter(view: self)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func btnJoinGroupPressed(_ sender: Any) {
        var alert : UIAlertController = UIAlertController()
        if self.IsUserLogin(){
            idFriend = UserDefaults.standard.string(forKey: "IdUserLogin") ?? ""
            self.joingGroupPresenter.joinMeInGroup(secretKey: txtSecretKeyGroup.text!, idUser: idFriend)
        }else{
           alert = createAlert(title: "El usuario no esta loguead", message: "El usuario no esta logueado en la aplicaciòn")
        }
    }

    
    private func createAlert(title:String,message:String) -> UIAlertController{
        
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction.init(title: "Ok", style: .default, handler: nil)
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        
        return alert
        
    }
    
    private func suscribeToNotificationUserLogin() -> Void{
        NotificationCenter.default.addObserver(self, selector: #selector(receivedInfoUser), name: NSNotification.Name("infoFriendLogin"), object: nil)
        
    }
    
    private func IsUserLogin()-> Bool{
        
        let isLogin = UserDefaults.standard.bool(forKey: "isLogin")
        if isLogin == true{
            return true
        }else{
            return false
        }
        
    }
    
    @objc private func receivedInfoUser(notification:Notification) -> Void{
        
        guard let userReceived = notification.object as? Friend else {
            return
        }
        print("este es el id recibido de la notificacion \(userReceived.id)")
        self.idFriend = userReceived.id!
    }
    
    private func createActiviindicator()-> UIActivityIndicatorView{
        
        let activityInd = self.activiIndicctorUtil.createActiviyIndicator(view: view)
        return activityInd
    }
}


extension InsertToGroupViewController : InserInGroupView{
    func showMessageInsertSuccesfully(title: String, message: String) {
        
        let alertCreated = self.createAlert(title: title, message: message)
        present(alertCreated, animated: true)
    }
    
    func showMessageInsertFailure(title: String, message: String) {
        let alertCreated = self.createAlert(title: title, message: message)
        present(alertCreated,animated: true)
    }
    
    func showActivityIndicator() {
        self.activiIndicatorView = createActiviindicator()
        self.activiIndicatorView.startAnimating()
    }
    
    func hideActivitiIndicator() {
        self.activiIndicctorUtil.removeActiviIndicatorView(view: view)
    }
}

