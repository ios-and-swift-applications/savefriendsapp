//
//  RegisterUserViewController.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/19/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import UIKit

class RegisterUserViewController: UIViewController {

    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    
    @IBOutlet weak var txtAge: UITextField!
    
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var txtPassword: UITextField!
    
    
    private var registerAccountPresenter : RegisterPresenter!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerAccountPresenter = RegisterPresenter(view: self)

        // Do any additional setup after loading the view.
    }
    
    @IBAction private func btnRegisterAccount(_ sender: Any) {
        var userRegister : Friend = Friend()
        userRegister.name = txtName.text
        userRegister.cellphone = txtPhone.text
        userRegister.age = txtAge.text
        userRegister.email = txtEmail.text
        userRegister.password = txtPassword.text
        userRegister.urlPhoto = ""
        
        registerAccountPresenter.RegisterUSer(user: userRegister)
        
    }
    
    private func createAlert(title:String,message:String)-> UIAlertController{
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
      
        let okAction = UIAlertAction.init(title: "OK", style: .default, handler :{ action in
            self.resetTextFields()
            self.gotoLoginPage()
        })
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        
        return alert
        
    }
    
    
    private func resetTextFields()-> Void{
        txtAge.text = ""
        txtName.text = ""
        txtEmail.text = ""
        txtPhone.text = ""
        txtPassword.text = ""
    }
}

extension RegisterUserViewController : RegisterView {
    func clearForm() {
        self.resetTextFields()
    }
    
    func showAlert(title:String, message: String) {
        
        var aler = createAlert(title: title, message: message)
        present(aler,animated: true)
        
    }
    
    func gotoLoginPage() {
        let loginVc = storyboard?.instantiateViewController(withIdentifier: "loginViewController") as! LoginViewController
        navigationController?.pushViewController(loginVc, animated: true)
    }
    
    func ValidEmail() {
        print("voy a login")
    }
    
    func validLongPassword() {
        print("voy a login")
    }
    
    
    
}


