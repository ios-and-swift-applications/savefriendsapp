//
//  LoginViewController.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/15/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnIngresar: UIButton!
    @IBOutlet weak var lbErrorMessage: UILabel!
    @IBOutlet var loginMainView: UIView!
    
    @IBOutlet weak var LoginView: UIView!
    private var activiIndicatorView : UIActivityIndicatorView!
   
    private var loginPresenter: LoginPresenter!
    private var registerPresenter : RegisterPresenter!
    var activitiIndicatorUtil : ActivityIndicatorUitil!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configUITextFielsLogin()
        self.loginPresenter = LoginPresenter(view: self)
        self.activitiIndicatorUtil = ActivityIndicatorUitil();
        
        
        txtEmail.text = "camilita@gmail.com"
        txtPassword.text = "camilita123"
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnLogin(_ sender: Any) {
        self.loginPresenter.login(email: txtEmail.text!, password: txtPassword.text!)
    }
    
    private func createActivityIndicator()-> UIActivityIndicatorView{
        
        let activityIndiControl = self.activitiIndicatorUtil.createActiviyIndicator(view: loginMainView)
        return activityIndiControl
    }
    private func configUITextFielsLogin()-> Void{
        txtEmail.delegate = self
        txtPassword.delegate = self
    }
    

}
extension LoginViewController : LoginView {
    
    func startActivityIndicator() {
        self.activiIndicatorView = createActivityIndicator()
        self.activiIndicatorView.startAnimating()
    }
    
    func stopActivityIndicator() {
        self.activiIndicatorView.stopAnimating()
    }
    
    func showMessage(title:String, message: String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler:{
            action in
                self.startActivityIndicator()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:nil)
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        present(alert,animated: true)
    }
    
    func gotoHomeView(friend:Friend) {
       
       UserDefaults.standard.set(true, forKey: "isLogin")
        UserDefaults.standard.set(friend.id, forKey: "IdUserLogin")
        let userAux = UserDefaults.standard.string(forKey: "IdUserLogin")
        print(userAux!)
       var myHomeView = self.storyboard?.instantiateViewController(withIdentifier: "homeTabBarViewController") as? UITabBarController
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = myHomeView
        
    }
    
    func gotoRegisterView() {
        print("Hola")
    }
    
    
}

/*
    MARK, Add Extension for UITEXTFIeld
 */


extension LoginViewController: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
