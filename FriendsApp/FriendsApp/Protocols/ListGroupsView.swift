//
//  ListGroupsView.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/22/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation

protocol ListGroupsView {
    
    
    func showActivityIndicator()-> Void
    func hideActivityIndicator()-> Void
    func ShowData(listaGroups:[Group])-> Void
    func gotoDeailGroupView(groupId:String)-> Void
    
}

