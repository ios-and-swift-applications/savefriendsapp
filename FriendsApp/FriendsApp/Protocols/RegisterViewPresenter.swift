//
//  RegisterViewPresenter.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/17/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation

protocol RegisterViewPresenter {
    
    init(view:RegisterView)
    func RegisterUSer(user:Friend) -> Void
    func IsValidLongitudPassword(password:String)-> Bool
    func IsValidEmailFormat(email:String)-> Bool
    func showAlert(title:String,message:String)-> Void
    
}
