//
//  InserInGroupViewPresenter.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/25/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation

protocol InserInGroupViewPresenter {
    
    
    init(view:InserInGroupView)
    
    func joinMeInGroup(secretKey:String,idUser:String) -> Void
    
}
