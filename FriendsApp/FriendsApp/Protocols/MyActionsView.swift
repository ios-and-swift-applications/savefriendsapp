//
//  MyActionsView.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/26/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation

protocol MyActionsView {
    
    func gotoMyGroups()-> Void
    func gotoJoinInGroup() -> Void
    func gotoGroupList()-> Void
    func ShowListMyActions()->Void
    
    
}
