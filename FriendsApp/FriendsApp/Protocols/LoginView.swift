//
//  LoginView.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/15/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation

protocol LoginView {
    
    func startActivityIndicator()-> Void
    func stopActivityIndicator()-> Void
    func showMessage(title:String,message:String)-> Void
    func gotoHomeView(friend:Friend)-> Void
    func gotoRegisterView() -> Void
}
