//
//  LoginViewPresenter.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/15/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation


protocol LoginViewPresenter {
    
    init(view:LoginView)
    func login(email:String,password:String)-> Void
    func ValidateEmailAndPassword(email:String?,password:String?) -> Bool
    func gotoRegisterView()-> Void
}
