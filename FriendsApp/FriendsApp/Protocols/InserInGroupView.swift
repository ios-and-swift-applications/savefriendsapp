//
//  InserInGroupView.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/25/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation

protocol InserInGroupView {
    
    
    func showMessageInsertSuccesfully(title:String,message:String)-> Void
    func showMessageInsertFailure(title:String,message:String)-> Void
    func showActivityIndicator()->Void
    func hideActivitiIndicator()->Void
    
}
