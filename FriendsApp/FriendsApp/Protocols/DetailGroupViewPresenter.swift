//
//  DetailGroupViewPresenter.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/24/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation


protocol DetailGroupViewPresenter {
    
    
    init(view:DetailGroupView)
    func getGroupById(idGroup:String)-> Void
    func joinMeInGroup(secretKey:String,idFriend:String) -> Void
    
}
