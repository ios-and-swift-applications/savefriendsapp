//
//  RegisterView.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/17/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation

protocol RegisterView {
    
    func clearForm() -> Void
    func showAlert(title:String, message:String) -> Void
    func gotoLoginPage() -> Void
    func ValidEmail() -> Void
    func validLongPassword() -> Void
}
