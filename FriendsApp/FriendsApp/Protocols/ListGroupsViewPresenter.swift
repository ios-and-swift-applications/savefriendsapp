//
//  ListGroupsViewPresenter.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/22/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation

protocol ListGroupsViewPresenter {
    
    init(view:ListGroupsView)
    func getListGroups()-> Void
    func getGroupById(idGroup:String) -> Void
    func viewGroupDetail(groupId:String) -> Void
    
    
}
