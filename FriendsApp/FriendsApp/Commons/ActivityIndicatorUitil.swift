//
//  ActivityIndicator.swift
//  FriendsApp
//
//  Created by Jorge luis Menco Jaraba on 12/30/18.
//  Copyright © 2018 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation
import UIKit

class ActivityIndicatorUitil {
    
    private  var container:UIView
    private  var activitiIndictaorView: UIView
    
    init() {
        container = UIView()
        activitiIndictaorView = UIView()
        
    }
    
    func createActiviyIndicator(view:UIView)-> UIActivityIndicatorView{
        
        
        container.frame = view.frame
        container.center = CGPoint(x: view.frame.size.width/2, y: view.frame.size.height/2)
        container.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.8)
        
        activitiIndictaorView.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        activitiIndictaorView.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.6)
        activitiIndictaorView.clipsToBounds = true
        activitiIndictaorView.layer.cornerRadius = 10;
        activitiIndictaorView.center = view.center
        
        let actIndicator = UIActivityIndicatorView()
        actIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        actIndicator.style = .white
        actIndicator.clipsToBounds = true
        actIndicator.center = CGPoint(x: activitiIndictaorView.frame.size.width/2, y: activitiIndictaorView.frame.size.height/2)
        
        activitiIndictaorView.addSubview(actIndicator)
        container.addSubview(activitiIndictaorView)
        view.addSubview(container)
        
        return actIndicator
        
    }
    
    func removeActiviIndicatorView(view:UIView)-> Void{
       
        container.isHidden = true
        view.addSubview(container)
    
    }
    
}

